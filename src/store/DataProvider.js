import { createContext, useState } from 'react'
import Axios from 'axios'
import constant from '../constant';
import axios from 'axios';

const AppContext = createContext({
    //state
    leftSideBarActiveButton: 1,
    leftSideBarMenuActive: true,

    savedHashTags: [],
    savedPage: 1,
    savedFinished: false,
    savedHashTagsLoading: true,

    groupName: "",
    category: [],
    tags: [],
    editId: null,

    //category data
    categoryData: [],
    categoryDataLoading: true,
    categoryDataError: false,

    groupesCreated: 0,
    tagsValueEdit: "",
    filterCategory: "",

    trending: [],
    trendingLoading: true,
    trendingError: false,
    trendCode: 1,

    instagram: [],
    instagramAccountsLoading: true,
    getInstagramAccounts: () => { },
    updateInstagramAccounts: () => { },

    // active organization
    activeOrganization: null,
    featureFactory: null,
    canEdit: false,
    getActiveOrganization: () => { },
    updateCredites: () => { },

    // titles
    titles: [],
    titlesLoading: true,
    getTitles: () => { },

    //functions
    changeLeftSideBarAciveButton: () => { },
    getTheSavedHashTags: () => { },
    removeDeletedHashtag: () => { },
    updateSavedHashtag: () => { },

    changeGroupName: () => { },
    changeTags: () => { },
    changeCategory: () => { },
    getGroupesCreated: () => { },
    changeTagsValueEdit: () => { },
    getCategory: () => { },
    updateCategoryData: () => { },
    changeFilterCategory: () => { },
    getTrendingTags: () => { },
    changeTrendCode: () => { },
    changeEditId: () => { }
})

export const AppContextProvider = (props) => {

    const [canEdit, setcanEdit] = useState(false)
    const [leftSideBarActiveButton, setleftSideBarActiveButton] = useState(1)

    const [savedHashTags, setsavedHashTags] = useState([])
    const [savedPage, setsavedPage] = useState(1)
    const [savedFinished, setsavedFinished] = useState(false)
    const [savedHashTagsLoading, setsavedHashTagsLoading] = useState(true)

    const [groupName, setgroupName] = useState("")
    const [tags, settags] = useState([])
    const [category, setcategory] = useState("")
    const [editId, seteditId] = useState(null)

    // category data
    const [categoryData, setcategoryData] = useState([])
    const [categoryDataLoading, setcategoryDataLoading] = useState(true)
    const [categoryDataError, setcategoryDataError] = useState(false)

    const [groupesCreated, setgroupesCreated] = useState(0)
    const [tagsValueEdit, settagsValueEdit] = useState("")
    const [filterCategory, setfilterCategory] = useState("")

    // trending
    const [trending, settrending] = useState([])
    const [trendingLoading, settrendingLoading] = useState(true)
    const [trendingError, settrendingError] = useState(false)
    const [trendCode, settrendCode] = useState(1)

    //active organization
    const [activeOrganization, setactiveOrganization] = useState(null)
    const [featureFactory, setfeatureFactory] = useState(null)

    const [instagram, setinstagram] = useState([])
    const [instagramAccountsLoading, setinstagramAccountsLoading] = useState(true)

    const [titles, setTitles] = useState([])
    const [titlesLoading, setTitlesLoading] = useState(true)

    const getTitles = async () => {
        try {
            setTitlesLoading(true)
            let workspaceId = localStorage.getItem("organizationId")
            const res = await axios.get(constant.url + "title/" + workspaceId)
            setTitles(res.data)
            setTitlesLoading(false)
        } catch (error) {
            setTitlesLoading(false)
        }
    }

    const changeEditId = (value) => {
        seteditId(value)
        setcategory("")
        setgroupName("")
    }

    const getInstagramAccounts = async () => {
        try {
            setinstagramAccountsLoading(true)
            const workspaceId = localStorage.getItem("organizationId")
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`https://api.cache.creatosaurus.io/cache/hashtag/instagram/accounts/${workspaceId}`, config)
            setinstagram(res.data)
            setinstagramAccountsLoading(false)
        } catch (error) {
            setinstagramAccountsLoading(false)
        }
    }

    const updateInstagramAccounts = (data) => {
        setinstagram(data)
    }

    const getActiveOrganization = async () => {
        try {
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            const res = await Axios.get('https://api.app.creatosaurus.io/creatosaurus/user/info', config)

            let userDetails = res.data.activeWorkspace.team.filter(data => data.user_email === res.data.userData.email)
            if (userDetails[0].role !== "view") {
                setcanEdit(true)
            } else {
                setcanEdit(false)
            }

            localStorage.removeItem("organizationId")
            localStorage.removeItem("organizationName")
            localStorage.setItem("organizationId", res.data.activeWorkspace._id)
            localStorage.setItem("organizationName", res.data.activeWorkspace.workspace_name)
            setactiveOrganization(res.data.activeWorkspace)
            setfeatureFactory(res.data.featureFactoryData)

            getGroupesCreated()
            getCategory()
            getTheSavedHashTags()
            getTrendingTags()
            getInstagramAccounts()
            getTitles()
        } catch (error) {
            getGroupesCreated()
            getCategory()
            getTheSavedHashTags()
            getTrendingTags()
            getInstagramAccounts()
            getTitles()
        }
    }

    const updateCredites = (number) => {
        let featureFactoryData = featureFactory
        featureFactoryData.hashtagSearch = number
        setfeatureFactory(featureFactoryData)
    }

    const changeLeftSideBarAciveButton = (value) => {
        setleftSideBarActiveButton(value)
    }

    const changeFilterCategory = (value) => {
        setfilterCategory(value)
    }

    const getTheSavedHashTags = async () => {
        try {
            setsavedHashTagsLoading(true)
            let organizationId = localStorage.getItem('organizationId')
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`${constant.url}${organizationId}?page=${savedPage}`, config)
            if (res.data.length < 10) setsavedFinished(true)
            setsavedHashTags((prev) => [...prev, ...res.data])
            setsavedHashTagsLoading(false)
            setsavedPage((prev) => prev + 1)
        } catch (error) {
            setsavedHashTagsLoading(false)
        }
    }

    const removeDeletedHashtag = (id) => {
        let filterData = savedHashTags.filter(data => data._id !== id)
        setsavedHashTags(filterData)
    }

    const getGroupesCreated = async () => {
        try {
            let organizationId = localStorage.getItem('organizationId')
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`${constant.url}/groupes/${organizationId}`, config)
            setgroupesCreated(res.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    const changeGroupName = (value) => {
        setgroupName(value)
    }

    const changeTags = (value) => {
        settags(value)
    }

    const changeCategory = (value) => {
        setcategory(value)
    }

    const changeTagsValueEdit = (value) => {
        settagsValueEdit(value)
    }

    const sortCategory = (data) => {
        let sortedData = data.sort((a, b) => a.category.localeCompare(b.category))
        return sortedData
    }

    const getCategory = async () => {
        try {
            if (categoryData.length !== 0) return
            setcategoryDataLoading(true)
            setcategoryDataError(false)
            let organizationId = localStorage.getItem('organizationId')
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`${constant.url}category/${organizationId}`, config)
            let data = sortCategory(res.data)
            setcategoryData(data)
            setcategoryDataLoading(false)
        } catch (error) {
            setcategoryDataError(true)
            setcategoryDataLoading(false)
        }
    }

    const updateCategoryData = (data) => {
        let filterData = sortCategory([data, ...categoryData])
        setcategoryData(filterData)
    }

    const getTrendingTags = async () => {
        try {
            if (trending.length !== 0) return
            settrendingLoading(true)
            settrendingError(false)
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            let res = await axios.get(`https://api.cache.creatosaurus.io/cache/twitter/trends?woeid=${trendCode}`, config)
            settrending(res.data[0].trends)
            settrendingLoading(false)
        } catch (error) {
            settrendingError(true)
            settrendingLoading(false)
        }
    }

    const changeTrendCode = async (value) => {
        try {
            settrendCode(value)
            settrendingLoading(true)
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            let res = await axios.get(`https://api.cache.creatosaurus.io/cache/twitter/trends?woeid=${value}`, config)
            settrending(res.data[0].trends)
            settrendingLoading(false)
        } catch (error) {
            settrendingLoading(false)
        }
    }

    const updateSavedHashtag = (value) => {
        setsavedHashTags((prev) => [value, ...prev])
    }

    const context = {
        //state
        leftSideBarActiveButton: leftSideBarActiveButton,
        groupesCreated: groupesCreated,
        canEdit: canEdit,

        savedHashTags: savedHashTags,
        savedPage: savedPage,
        savedFinished: savedFinished,
        savedHashTagsLoading: savedHashTagsLoading,

        groupName: groupName,
        tags: tags,
        category: category,
        editId: editId,
        tagsValueEdit: tagsValueEdit,
        filterCategory: filterCategory,

        //category data
        categoryData: categoryData,
        categoryDataLoading: categoryDataLoading,
        categoryDataError: categoryDataError,

        //trendings
        trending: trending,
        trendingLoading: trendingLoading,
        trendingError: trendingError,
        trendCode: trendCode,

        // titles
        titles: titles,
        titlesLoading: titlesLoading,

        // active organization
        activeOrganization: activeOrganization,
        featureFactory: featureFactory,
        getActiveOrganization: getActiveOrganization,
        updateCredites: updateCredites,

        instagram: instagram,
        instagramAccountsLoading: instagramAccountsLoading,
        getInstagramAccounts: getInstagramAccounts,
        updateInstagramAccounts: updateInstagramAccounts,

        //functions
        changeLeftSideBarAciveButton: changeLeftSideBarAciveButton,
        getTheSavedHashTags: getTheSavedHashTags,

        changeGroupName: changeGroupName,
        changeTags: changeTags,
        changeCategory: changeCategory,
        getGroupesCreated: getGroupesCreated,
        changeTagsValueEdit: changeTagsValueEdit,
        getCategory: getCategory,
        updateCategoryData: updateCategoryData,
        removeDeletedHashtag: removeDeletedHashtag,
        changeFilterCategory: changeFilterCategory,
        getTrendingTags: getTrendingTags,
        changeTrendCode: changeTrendCode,
        changeEditId: changeEditId,
        updateSavedHashtag: updateSavedHashtag
    }

    return <AppContext.Provider value={context}>
        {props.children}
    </AppContext.Provider>
}

export default AppContext