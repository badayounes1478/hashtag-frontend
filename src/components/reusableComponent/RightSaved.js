import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../reusableComponentCss/RightSaved.css'

const RightSaved = () => {

    const context = useContext(AppContext)

    return (
        <div className="right-saved">
            <div className='title'>Filter</div>
            {
                context.categoryDataLoading ? <div style={{ textAlign: 'center' }}>Loading . . .</div> :
                    <select
                        value={context.filterCategory}
                        onChange={(e) => context.changeFilterCategory(e.target.value)}>
                        <option value="">All</option>
                        {
                            context.categoryData.map(data => {
                                return <option key={data._id} value={data.category}>{data.category}</option>
                            })
                        }
                    </select>
            }
        </div>
    )
}

export default RightSaved
