import React, { useState, useContext } from 'react'
import '../reusableComponentCss/ConnectAccountPopup.css'
import Instagram from '../../assets/Instagram.svg'
import Axios from 'axios'
import AppContext from '../../store/DataProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ConnectAccountPopup = (props) => {

    const [loading, setloading] = useState(false)
    const context = useContext(AppContext)

    const checkActiveAccountIsThere = () => {
        let data = props.data.filter(data => data.active === true && data.hide !== true)
        if (data.length === 0) return false
        return true
    }

    const info = [{
        clickHere: false,
        p: 'For the moment only Instagram Business accounts that have been connected to a Facebook Page and are managed by you are supported.'
    }, {
        clickHere: 'https://help.instagram.com/502981923235522',
        p: 'To convert your Instagram Personal account to a Business account,'
    }, {
        clickHere: 'https://help.instagram.com/399237934150902',
        p: 'To connect your Facebook Page to your Instagram Business account,'
    }, {
        clickHere: '#1',
        p: 'Check out our Instagram Business account connection guide for more information,'
    }]


    const saveTheInstagramData = async () => {
        try {
            if (checkActiveAccountIsThere() === false) return
            setloading(true);
            const workspaceId = localStorage.getItem('organizationId');
            let userSelectedAccounts = props.data.filter(data => data.active === true && data.hide !== true)

            userSelectedAccounts = userSelectedAccounts.map(data => {
                data.active = true
                data.type = "instagram"
                return data
            })

            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            await Axios.post("https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/cache/remove/credit/account", {
                id: workspaceId,
                accountsToConnect: userSelectedAccounts.length
            }, config)

            const res = await Axios.post(`https://api.cache.creatosaurus.io/cache/accounts/create`, {
                workspaceId: workspaceId,
                accounts: userSelectedAccounts
            }, config)

            let connectedAccounts = res.data.accounts.filter(data => data.accountType === "instagram")
            context.updateInstagramAccounts(connectedAccounts)

            setloading(false);
            props.close("new accounts conected")
            props.instagramAccount(connectedAccounts[connectedAccounts.length - 1].socialId)
        } catch (error) {
            toast.error(error.response.data.error)
            setloading(false);
            props.close()
        }
    };

    const checkAllAccountConnected = () => {
        let count = 0
        props.data.forEach(account => {
            if (account.hide) {
                count = count + 1
            }
        });
        if (props.data.length === count) return true
        return false
    }

    return (
        <div className='connect-account' onClick={() => props.close()}>
            <div className='popup' onClick={(e) => e.stopPropagation()}>
                <div className='head'>
                    <span>Instagram Bussiness Accounts</span>
                    <div className='button-container'>
                        {
                            checkActiveAccountIsThere() ?
                                <button
                                    onClick={loading ? null : saveTheInstagramData}
                                    className='save active'>{loading ? "Saving" : "Save"}</button> : null
                        }

                        <button className="close" onClick={() => props.close()}>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.0007 9.99973L14.3695 14.3686M5.63184 14.3686L10.0007 9.99973L5.63184 14.3686ZM14.3695 5.63086L10.0007 9.99973L14.3695 5.63086ZM10.0007 9.99973L5.63184 5.63086L10.0007 9.99973Z" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>

                <div className='scroll'>
                    {
                        props.data.length === 0 ? <div className='account-not-found'>
                            <p>Instagram bussiness account not found.</p>
                            {

                                info.map((data, index) => {
                                    return <p key={index}>{data.p}
                                        <React.Fragment>
                                            {
                                                // checking in perticular paragraph link is there or not
                                                data.clickHere === false ?
                                                    null :
                                                    <a href={data.clickHere} target="_blank" rel="noreferrer"> click here.</a>
                                            }
                                        </React.Fragment>
                                    </p>
                                })
                            }
                        </div> :
                            checkAllAccountConnected() ? <div className='all-accounts-connected'>You have connected all your Instagram bussiness accounts.</div> :
                                props.data.map((data, index) => {
                                    return <div
                                        key={index + data.id}
                                        className='card'
                                        style={data.hide ? { display: 'none' } : null}>
                                        <div>
                                            <img src={data.profileURL === undefined ? Instagram : data.profileURL} alt="" />
                                            <span>{data.name}</span>
                                        </div>
                                        <button className={data.active ? "active" : null} onClick={() => props.toggle(index)}>{data.active ? "Connected" : "Click To Connect"}</button>
                                    </div>
                                })
                    }
                </div>
            </div>
            <ToastContainer />
        </div>
    )
}

export default ConnectAccountPopup