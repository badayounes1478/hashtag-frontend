import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../reusableComponentCss/CenterDashBoard.css'
import { ToastContainer } from 'react-toastify'
import CenterSavedCard from '../card/CenterSavedCard'

const CenterDashBoard = () => {
    const context = useContext(AppContext)

    const Loading = () => {
        return <div className='scroll-container'>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div key={data} className="loading-card">
                        <div className='skeleton' style={{ width: "100%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                    </div>
                })
            }
        </div>
    }

    return (
        <div className="center-dashboard">
            <input id="copySaved" type="text" value="" readOnly={true} />
            <div className="search-container">
                <div>
                    <p>Decide the effect you want to<br />produce in your reader.</p>
                    <span>– Robert Collier</span>
                </div>
                <div className="input-container">
                    <button onClick={() => context.changeLeftSideBarAciveButton(2)}>Create collection</button>
                </div>
            </div>

            <div className="quotes-info">
                <div><span>{context.savedHashTagsLoading ? 0 : context.savedHashTags.length}</span>saved</div>
                <div className="line" />
                <div><span>{context.categoryData.length}</span>groups created</div>
                <div className="line" />
                <div><span>{context.savedHashTagsLoading ? 0 : context.savedHashTags.length * 3}</span>mins saved</div>
            </div>

            <div className="popular-quotes">
                <input id="copySaved" type="text" value="" readOnly={true} />
                {
                    context.savedHashTagsLoading ? <Loading /> :
                        context.savedHashTags.length === 0 && context.savedHashTagsLoading !== true ?
                            <div style={{ marginTop: 10, textAlign: 'center' }}>You haven't saved anything.</div> :
                            context.savedHashTags.map((data, index) => {
                                if (context.filterCategory === "") {
                                    return <CenterSavedCard key={data._id + index} data={data} />
                                } else if (context.filterCategory === data.category) {
                                    return <CenterSavedCard key={data._id + index} data={data} />
                                } else {
                                    return null
                                }
                            })
                }
            </div>
            <ToastContainer />
        </div>
    )
}

export default CenterDashBoard
