import React, { useContext } from 'react'
import '../reusableComponentCss/RightDashBoard.css'
import AppContext from '../../store/DataProvider'

const RightDashBoard = () => {
    const context = useContext(AppContext)

    const Loading = () => {
        return <div className='scroll-container'>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div key={data} className="loading-card">
                        <div className='skeleton' style={{ width: "50%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 15 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                    </div>
                })
            }
        </div>
    }

    return (
        <div className="right-dashboard">
            <input id="copySaved" type="text" value="" readOnly={true} />
            <div className="join-community">
                <span>Looking for a little assistance?</span>
                <a href="https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht" target="_blank" rel="noopener noreferrer">
                    Live chat support
                </a>
                <a href="https://youtu.be/eS5tpAUEuzA" target="_blank" rel="noopener noreferrer">
                    Video Tutorial
                </a>
                <a href="https://www.facebook.com/groups/creatosaurus/" target="_blank" rel="noopener noreferrer">
                    Join our Facebook group
                </a>
            </div>
            <div className="recent-saved">
                <div className="head">
                    <span>Instagram Business Accounts</span>
                </div>
                <div className='card-wrapper'>
                    {
                        context.instagramAccountsLoading ?
                            <Loading /> :
                            context.instagram.length === 0 ?
                                <span className='no-account'>No Account's Found</span> :
                                context.instagram.map((data, index) => {
                                    return <div className='card'>
                                        <img src={data.profileURL} alt='' />
                                        <span>{data.name}</span>
                                    </div>
                                })
                    }
                </div>
            </div>
        </div>
    )
}

export default RightDashBoard
