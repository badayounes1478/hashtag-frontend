import React, { useContext } from 'react'
import '../reusableComponentCss/RightContainer.css'
import RightDashBoard from './RightDashBoard'
import AppContext from '../../store/DataProvider'
import RightSaved from './RightSaved'
import RightCreateCategory from './RightCreateCategory'

const RightContainer = () => {

  const context = useContext(AppContext)

  return (
    <>
      {
        context.leftSideBarActiveButton === 6 ? null :
          <div className="right-container">
            {
              context.leftSideBarActiveButton === 1 ? <RightDashBoard /> :
                context.leftSideBarActiveButton === 2 ? <RightCreateCategory /> :
                  context.leftSideBarActiveButton === 3 ? <RightSaved /> : <RightDashBoard />
            }
          </div>
      }
    </>
  )
}

export default RightContainer
