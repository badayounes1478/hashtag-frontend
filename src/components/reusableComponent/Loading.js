import React from 'react'
import '../reusableComponentCss/Loading.css'

const Loading = () => {
    return (
        <div className='loader-container-component'>
            <div className='center'>
                <div className='loader-component'>
                    <div className='ball'></div>
                    <div className='ball'></div>
                    <div className='ball'></div>
                </div>
                <span>Loading your Instagram business accounts</span>
            </div>
        </div>
    )
}

export default Loading