import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../screencss/Settings.css'

const Setting = () => {
    const context = useContext(AppContext)

    return (
        <div className='settings-container'>
            <div className='setting-toggle-container'>
                <button>Plans</button>
            </div>
            <div className='plan-details'>
                <h1>{context.featureFactory?.planId?.planName} Plan</h1>
                <div className="plansParaOne">
                    <p>Number of hashtag groupes you can create</p>
                    <button>5</button>
                </div>

                <div className="plansParaOne">
                    <p>Search for hashtag suggestions</p>
                    <button>{context.featureFactory?.planId?.planName !== "free" ? "Yes" : "No"}</button>
                </div>

                <div className="plansParaOne" style={{ border: 'none' }}>
                    <p>Twitter Trends</p>
                    <button>Yes</button>
                </div>
                <button
                    onClick={() => window.open("https://www.creatosaurus.io/pricing")}
                    className="upgradeBtn">Upgrade to Increase Limits</button>
            </div>
        </div>
    )
}

export default Setting